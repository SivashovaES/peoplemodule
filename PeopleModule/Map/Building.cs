﻿using CityDataExpansion.OsmGeometris;
using NetTopologySuite.Geometries;

namespace PeopleModule
{
    public class Building : OsmClosedWay
    {
        public Building(OsmNode[] nodes) : base(nodes)
        {
            
        }

        public Coordinate Entrance { get; set; }
    }
}

﻿
namespace PeopleModule
{
    static class ModuleConstants
    {
        public const string SettingsDirectoryPath = @"settings/";
        public const string PresetsFilePath = SettingsDirectoryPath + "Presets";
        public const string OsmFolderPath = @"maps/";
        public const string OsmFileName = "map.osm";
        public const string ModulesDirectoryPath = @"modules/";
    }
}

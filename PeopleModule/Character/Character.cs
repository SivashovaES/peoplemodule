﻿using NetTopologySuite.Geometries;
using System;
using OSMLSGlobalLibrary.Map;
using CityDataExpansion.OsmGeometris;

namespace PeopleModule
{
    [CustomStyle(@"new ol.style.Style({
                image: new ol.style.Circle({
                    opacity: 1.0,
                    scale: 1.0,
                    radius: 3,
                    fill: new ol.style.Fill({
                      color: 'rgba(255, 0, 0, 0.9)'
                    }),
                    stroke: new ol.style.Stroke({
                      color: 'rgba(0, 0, 0, 0.4)',
                      width: 1
                    }),
                })
            });
        ")]
    public class Character : Point
    {
        /// <summary>
        /// Actor's path
        /// </summary>
        public LineString Path { get; set; }

        /// <summary>
        /// Current coordinate
        /// </summary>
        public Coordinate Point { get; }

        /// <summary>
        /// Next coordinate
        /// </summary>
        public Coordinate NextPoint { get; set; }

        //no deseleration if 0
        public int Deceleration { get; set; }

        /// <summary>
        /// Speed
        /// </summary>
        public int Speed { get; } = 1;

        /// <summary>
        /// Roadlines of current map
        /// </summary>
        private OsmWay[] RoadLines;

        private Random Rnd;

        // ==================== Personal properties ============================

        /// <summary>
        /// Person's age
        /// </summary>
        public int Age { get; }
       
        /// <summary>
        /// ID of actor
        /// </summary>
        public int ID { get; }

        /// <summary>
        /// Person's health
        /// true - good, false - bad
        /// </summary>
        public bool Health { get; }

        // =========================== Methods =================================

        /// <summary>
        /// Detecting other actors in radius R
        /// <param name="SP">source point</param>
        /// <param name="TP">target point</param>
        /// <param name="R">radius</param>
        /// </summary>
        public static bool CollisionCharaster(Coordinate SP, Coordinate TP, int R)
        {
            bool collision = false;

            double S = Math.Sqrt(Math.Pow((TP.X - SP.X), 2) + Math.Pow((TP.Y - SP.Y), 2));
            //collision = S <= R ? true : false;
            if (S <= R)
            {
                collision = true;
            }
            //else collision = false;

            return collision;
        }

        public Character(Coordinate start, LineString path, int deceleration, bool health, int age, OsmWay[] roadlines, int ID) : base(start)
        {
            this.RoadLines = roadlines;
            this.Path = path;
            Point = path.StartPoint.Coordinate;
            NextPoint = path.NumPoints != 1 ? path.Coordinates[1] : path.Coordinates[0];
            X = Point.X;
            Y = Point.Y;
            this.ID = ID;
            this.Health = health;
            this.Age = age;
            Deceleration = deceleration;

            Rnd = new Random();
        }

        public Character(Coordinate coordinate) : base(coordinate)
        {
        }

        private static bool comparePoints(Point EndPoint, Point p2)
        {
            return (EndPoint.X == p2.X && EndPoint.Y == p2.Y);
        }

        private static bool comparePointWithZero(Coordinate p)
        {
            return (p.X == 0 && p.Y == 0);
        }

        public bool WillBeUpdated()
        {
            return Rnd.Next(Deceleration) == 0;
        }
        public void Update(out bool remove)
        {
            if (WillBeUpdated())
            {
                if (!comparePointWithZero(NextPoint))
                {

                    if (move())
                    {
                        NextPoint = getNextNode();
                    }
                    remove = false;
                }
                else
                {
                    remove = true;
                }
            }
            else
            {
                remove = false;
            }

            X = Point.X;
            Y = Point.Y;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>next node reached</returns>
        private bool move()
        {
            if (Math.Abs(Point.X - NextPoint.X) < 1 && Math.Abs(Point.Y - NextPoint.Y) < 1)
                return true;

            if (Point.X != NextPoint.X)
            {
                Point.X += Point.X < NextPoint.X ? Speed : -Speed;
            }
            if (Point.Y != NextPoint.Y)
            {
                Point.Y += Point.Y < NextPoint.Y ? Speed : -Speed;
            }

            return false;
        }

        
        private Coordinate getNextNode()
        {
            int indexNext = Array.IndexOf(Path.Coordinates, NextPoint) + 1;
            return indexNext != Path.Count ? Path.Coordinates[indexNext] : new Coordinate(0,0);
        }
    }
}

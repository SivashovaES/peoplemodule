﻿using System;
using NetTopologySuite.Geometries;
using NetTopologySuite.Mathematics;
using System.Globalization;
using CityDataExpansion.OsmGeometris;

namespace PeopleModule
{
    class ModuleMathExtensions
    {
        //It is unacceptable to store these values here, need to store it for each vector of UTM coordinates
        //But another way we need to store a lot of useless data
        static int Zone;
        static char Letter;

        public static Point AreaExtension { get; private set; } = new Point(1000, 1000);

        public static Coordinate LineCenter(LineSegment line)
        {
            return new Coordinate((line.P0.X + line.P1.X) / 2, (line.P0.Y + line.P1.Y) / 2);
        }

        public static Vector2D Deg2UTM(double lat, double lon)
        {
            double easting;
            double northing;
            int zone;
            char letter;

            zone = (int)Math.Floor(lon / 6 + 31);
            if (lat < -72)
                letter = 'C';
            else if (lat < -64)
                letter = 'D';
            else if (lat < -56)
                letter = 'E';
            else if (lat < -48)
                letter = 'F';
            else if (lat < -40)
                letter = 'G';
            else if (lat < -32)
                letter = 'H';
            else if (lat < -24)
                letter = 'J';
            else if (lat < -16)
                letter = 'K';
            else if (lat < -8)
                letter = 'L';
            else if (lat < 0)
                letter = 'M';
            else if (lat < 8)
                letter = 'N';
            else if (lat < 16)
                letter = 'P';
            else if (lat < 24)
                letter = 'Q';
            else if (lat < 32)
                letter = 'R';
            else if (lat < 40)
                letter = 'S';
            else if (lat < 48)
                letter = 'T';
            else if (lat < 56)
                letter = 'U';
            else if (lat < 64)
                letter = 'V';
            else if (lat < 72)
                letter = 'W';
            else
                letter = 'X';
            easting = 0.5 * Math.Log((1 + Math.Cos(lat * Math.PI / 180) * Math.Sin(lon * Math.PI / 180 - (6 * zone - 183) * Math.PI / 180)) / (1 - Math.Cos(lat * Math.PI / 180) * Math.Sin(lon * Math.PI / 180 - (6 * zone - 183) * Math.PI / 180))) * 0.9996 * 6399593.62 / Math.Pow((1 + Math.Pow(0.0820944379, 2) * Math.Pow(Math.Cos(lat * Math.PI / 180), 2)), 0.5) * (1 + Math.Pow(0.0820944379, 2) / 2 * Math.Pow((0.5 * Math.Log((1 + Math.Cos(lat * Math.PI / 180) * Math.Sin(lon * Math.PI / 180 - (6 * zone - 183) * Math.PI / 180)) / (1 - Math.Cos(lat * Math.PI / 180) * Math.Sin(lon * Math.PI / 180 - (6 * zone - 183) * Math.PI / 180)))), 2) * Math.Pow(Math.Cos(lat * Math.PI / 180), 2) / 3) + 500000;
            easting = Math.Round(easting * 100) * 0.01;
            northing = (Math.Atan(Math.Tan(lat * Math.PI / 180) / Math.Cos((lon * Math.PI / 180 - (6 * zone - 183) * Math.PI / 180))) - lat * Math.PI / 180) * 0.9996 * 6399593.625 / Math.Sqrt(1 + 0.006739496742 * Math.Pow(Math.Cos(lat * Math.PI / 180), 2)) * (1 + 0.006739496742 / 2 * Math.Pow(0.5 * Math.Log((1 + Math.Cos(lat * Math.PI / 180) * Math.Sin((lon * Math.PI / 180 - (6 * zone - 183) * Math.PI / 180))) / (1 - Math.Cos(lat * Math.PI / 180) * Math.Sin((lon * Math.PI / 180 - (6 * zone - 183) * Math.PI / 180)))), 2) * Math.Pow(Math.Cos(lat * Math.PI / 180), 2)) + 0.9996 * 6399593.625 * (lat * Math.PI / 180 - 0.005054622556 * (lat * Math.PI / 180 + Math.Sin(2 * lat * Math.PI / 180) / 2) + 4.258201531e-05 * (3 * (lat * Math.PI / 180 + Math.Sin(2 * lat * Math.PI / 180) / 2) + Math.Sin(2 * lat * Math.PI / 180) * Math.Pow(Math.Cos(lat * Math.PI / 180), 2)) / 4 - 1.674057895e-07 * (5 * (3 * (lat * Math.PI / 180 + Math.Sin(2 * lat * Math.PI / 180) / 2) + Math.Sin(2 * lat * Math.PI / 180) * Math.Pow(Math.Cos(lat * Math.PI / 180), 2)) / 4 + Math.Sin(2 * lat * Math.PI / 180) * Math.Pow(Math.Cos(lat * Math.PI / 180), 2) * Math.Pow(Math.Cos(lat * Math.PI / 180), 2)) / 3);
            if (letter < 'M')
                northing = northing + 10000000;
            northing = Math.Round(northing * 100) * 0.01;

            Zone = zone;
            Letter = letter;

            return new Vector2D(Convert.ToSingle(easting), Convert.ToSingle(northing));

        }

        public static Vector2D Deg2UTM(NodeXml node)
        {
            return Deg2UTM(float.Parse(node.Lat, NumberStyles.Any, CultureInfo.InvariantCulture),
                float.Parse(node.Lon, NumberStyles.Any, CultureInfo.InvariantCulture));
        }

        public static Vector2D UTM2Deg(Vector2D UTM)
        {
            int zone = Zone;
            char letter = Letter;
            double easting = UTM.X;
            double northing = UTM.Y;
            double hem;
            if (letter > 'M')
                hem = 'N';
            else
                hem = 'S';
            double north;
            if (hem == 'S')
                north = northing - 10000000;
            else
                north = northing;
            var latitude = (north / 6366197.724 / 0.9996 + (1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2) - 0.006739496742 * Math.Sin(north / 6366197.724 / 0.9996) * Math.Cos(north / 6366197.724 / 0.9996) * (Math.Atan(Math.Cos(Math.Atan((Math.Exp((easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))) * (1 - 0.006739496742 * Math.Pow((easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))), 2) / 2 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2) / 3)) - Math.Exp(-(easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))) * (1 - 0.006739496742 * Math.Pow((easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))), 2) / 2 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2) / 3))) / 2 / Math.Cos((north - 0.9996 * 6399593.625 * (north / 6366197.724 / 0.9996 - 0.006739496742 * 3 / 4 * (north / 6366197.724 / 0.9996 + Math.Sin(2 * north / 6366197.724 / 0.9996) / 2) + Math.Pow(0.006739496742 * 3 / 4, 2) * 5 / 3 * (3 * (north / 6366197.724 / 0.9996 + Math.Sin(2 * north / 6366197.724 / 0.9996) / 2) + Math.Sin(2 * north / 6366197.724 / 0.9996) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) / 4 - Math.Pow(0.006739496742 * 3 / 4, 3) * 35 / 27 * (5 * (3 * (north / 6366197.724 / 0.9996 + Math.Sin(2 * north / 6366197.724 / 0.9996) / 2) + Math.Sin(2 * north / 6366197.724 / 0.9996) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) / 4 + Math.Sin(2 * north / 6366197.724 / 0.9996) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) / 3)) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))) * (1 - 0.006739496742 * Math.Pow((easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))), 2) / 2 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) + north / 6366197.724 / 0.9996))) * Math.Tan((north - 0.9996 * 6399593.625 * (north / 6366197.724 / 0.9996 - 0.006739496742 * 3 / 4 * (north / 6366197.724 / 0.9996 + Math.Sin(2 * north / 6366197.724 / 0.9996) / 2) + Math.Pow(0.006739496742 * 3 / 4, 2) * 5 / 3 * (3 * (north / 6366197.724 / 0.9996 + Math.Sin(2 * north / 6366197.724 / 0.9996) / 2) + Math.Sin(2 * north / 6366197.724 / 0.9996) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) / 4 - Math.Pow(0.006739496742 * 3 / 4, 3) * 35 / 27 * (5 * (3 * (north / 6366197.724 / 0.9996 + Math.Sin(2 * north / 6366197.724 / 0.9996) / 2) + Math.Sin(2 * north / 6366197.724 / 0.9996) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) / 4 + Math.Sin(2 * north / 6366197.724 / 0.9996) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) / 3)) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))) * (1 - 0.006739496742 * Math.Pow((easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))), 2) / 2 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) + north / 6366197.724 / 0.9996)) - north / 6366197.724 / 0.9996) * 3 / 2) * (Math.Atan(Math.Cos(Math.Atan((Math.Exp((easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))) * (1 - 0.006739496742 * Math.Pow((easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))), 2) / 2 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2) / 3)) - Math.Exp(-(easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))) * (1 - 0.006739496742 * Math.Pow((easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))), 2) / 2 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2) / 3))) / 2 / Math.Cos((north - 0.9996 * 6399593.625 * (north / 6366197.724 / 0.9996 - 0.006739496742 * 3 / 4 * (north / 6366197.724 / 0.9996 + Math.Sin(2 * north / 6366197.724 / 0.9996) / 2) + Math.Pow(0.006739496742 * 3 / 4, 2) * 5 / 3 * (3 * (north / 6366197.724 / 0.9996 + Math.Sin(2 * north / 6366197.724 / 0.9996) / 2) + Math.Sin(2 * north / 6366197.724 / 0.9996) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) / 4 - Math.Pow(0.006739496742 * 3 / 4, 3) * 35 / 27 * (5 * (3 * (north / 6366197.724 / 0.9996 + Math.Sin(2 * north / 6366197.724 / 0.9996) / 2) + Math.Sin(2 * north / 6366197.724 / 0.9996) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) / 4 + Math.Sin(2 * north / 6366197.724 / 0.9996) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) / 3)) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))) * (1 - 0.006739496742 * Math.Pow((easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))), 2) / 2 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) + north / 6366197.724 / 0.9996))) * Math.Tan((north - 0.9996 * 6399593.625 * (north / 6366197.724 / 0.9996 - 0.006739496742 * 3 / 4 * (north / 6366197.724 / 0.9996 + Math.Sin(2 * north / 6366197.724 / 0.9996) / 2) + Math.Pow(0.006739496742 * 3 / 4, 2) * 5 / 3 * (3 * (north / 6366197.724 / 0.9996 + Math.Sin(2 * north / 6366197.724 / 0.9996) / 2) + Math.Sin(2 * north / 6366197.724 / 0.9996) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) / 4 - Math.Pow(0.006739496742 * 3 / 4, 3) * 35 / 27 * (5 * (3 * (north / 6366197.724 / 0.9996 + Math.Sin(2 * north / 6366197.724 / 0.9996) / 2) + Math.Sin(2 * north / 6366197.724 / 0.9996) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) / 4 + Math.Sin(2 * north / 6366197.724 / 0.9996) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) / 3)) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))) * (1 - 0.006739496742 * Math.Pow((easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))), 2) / 2 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) + north / 6366197.724 / 0.9996)) - north / 6366197.724 / 0.9996)) * 180 / Math.PI;
            latitude = Math.Round(latitude * 10000000);
            latitude = latitude / 10000000;
            var longitude = Math.Atan((Math.Exp((easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))) * (1 - 0.006739496742 * Math.Pow((easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))), 2) / 2 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2) / 3)) - Math.Exp(-(easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))) * (1 - 0.006739496742 * Math.Pow((easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))), 2) / 2 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2) / 3))) / 2 / Math.Cos((north - 0.9996 * 6399593.625 * (north / 6366197.724 / 0.9996 - 0.006739496742 * 3 / 4 * (north / 6366197.724 / 0.9996 + Math.Sin(2 * north / 6366197.724 / 0.9996) / 2) + Math.Pow(0.006739496742 * 3 / 4, 2) * 5 / 3 * (3 * (north / 6366197.724 / 0.9996 + Math.Sin(2 * north / 6366197.724 / 0.9996) / 2) + Math.Sin(2 * north / 6366197.724 / 0.9996) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) / 4 - Math.Pow(0.006739496742 * 3 / 4, 3) * 35 / 27 * (5 * (3 * (north / 6366197.724 / 0.9996 + Math.Sin(2 * north / 6366197.724 / 0.9996) / 2) + Math.Sin(2 * north / 6366197.724 / 0.9996) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) / 4 + Math.Sin(2 * north / 6366197.724 / 0.9996) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2) * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) / 3)) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))) * (1 - 0.006739496742 * Math.Pow((easting - 500000) / (0.9996 * 6399593.625 / Math.Sqrt((1 + 0.006739496742 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)))), 2) / 2 * Math.Pow(Math.Cos(north / 6366197.724 / 0.9996), 2)) + north / 6366197.724 / 0.9996)) * 180 / Math.PI + zone * 6 - 183;
            longitude = Math.Round(longitude * 10000000);
            longitude = longitude / 10000000;

            return new Vector2D(Convert.ToSingle(latitude), Convert.ToSingle(longitude));
        }



        public static bool CheckintersectsLines(OsmWay[] roadlines, Coordinate cpoint, Coordinate npoint)
        {
            bool intersects = false;

            OsmNode[] nodes = new OsmNode[] { new OsmNode(cpoint), new OsmNode(npoint) };
            // build line from 2 points of path 
            OsmWay P = new OsmWay(nodes);
            //
            foreach (var rl in roadlines)
            {
                // find intersect of 2 lines 
                intersects = (rl.Intersection(P) != null);
                if (intersects)
                {
                    break;
                }
            }
            return intersects;
        }

        public static Coordinate FindPointIntersects(OsmWay firstLines, OsmWay secondLines)
        {
            Coordinate intersects;
            double ixOut, iyOut;
            double X1 = firstLines.StartPoint.X;
            double Y1 = firstLines.StartPoint.Y;
            double X2 = firstLines.EndPoint.X;
            double Y2 = firstLines.EndPoint.Y;
            // two lines
            double X3 = secondLines.StartPoint.X;
            double Y3 = secondLines.StartPoint.Y;
            double X4 = secondLines.EndPoint.X;
            double Y4 = secondLines.EndPoint.Y;

            double Det(double a, double b, double c, double d)
            {
                return a * d - b * c;
            }
            double detL1 = Det(X1, Y1, X2, Y2);
            double detL2 = Det(X3, Y3, X4, Y4);
            double X1mX2 = X1 - X2;
            double X3mX4 = X3 - X4;
            double Y1mY2 = Y1 - Y2;
            double Y3mY4 = Y3 - Y4;

            double xnom = Det(detL1, X1mX2, detL2, X3mX4);
            double ynom = Det(detL1, Y1mY2, detL2, Y3mY4);
            double denom = Det(X1mX2, Y1mY2, X3mX4, Y3mY4);

            ixOut = xnom / denom;
            iyOut = ynom / denom;

            intersects = new Coordinate(ixOut, iyOut);

            return intersects;
        }

        public static void AngleLines(OsmWay Flines, OsmWay Slines, out double angle)
        {
            double a = (180 / Math.PI);
            //line x, Y Roadlines
            double X1 = Flines.StartPoint.X;
            double Y1 = Flines.StartPoint.Y;
            double X2 = Flines.EndPoint.X;
            double Y2 = Flines.EndPoint.Y;
            //line x, y characters
            double X3 = Slines.StartPoint.X;
            double Y3 = Slines.StartPoint.Y;
            double X4 = Slines.EndPoint.X;
            double Y4 = Slines.EndPoint.Y;

            double R1 = Math.Atan2(Y1 - Y2, X1 - X2) * a;
            double A1 = Math.Atan2(Y3 - Y4, X3 - X4) * a;

            if ((R1 < 0) & (A1 > 0) | (R1 > 0) & (A1 < 0))
                angle = Math.Abs((180 - Math.Abs(R1)) - Math.Abs(A1));
            else angle = Math.Abs(R1 - A1);

        }

        public static double AngleRotate(double angle, double angledegrees)
        {
            double Rotate = angledegrees - angle;
            return Rotate;
        }

        public static Coordinate LineRotate(OsmWay linerotate, double anglerotate)
        {
            double X, Y;
            double X1 = linerotate.StartPoint.X;
            double Y1 = linerotate.StartPoint.Y;
            double X2 = linerotate.EndPoint.X;
            double Y2 = linerotate.EndPoint.Y;
            Coordinate rotate;

            double cosT = Math.Cos(Math.Abs(anglerotate * (Math.PI / 180)));
            double sinT = Math.Sin(Math.Abs(anglerotate * (Math.PI / 180)));

            X = (cosT * (X2 - X1) - sinT * (Y2 - Y1) + X2);
            Y = (sinT * (X2 - X1) + cosT * (Y2 - Y1) + Y2);

            rotate = new Coordinate(X, Y);

            return rotate;
        }

        /// <summary>
        /// Resize line
        /// </summary>
        /// <param name="sourceline"></param>
        /// <param name="resizelong"></param>
        /// <param name="flag"> flag 1 - all resize lines 2 - resize start point lines 3 - resize end point lines </param>
        /// <returns></returns>
        public OsmWay ResizeLine(OsmWay sourceline, int resizelong, int flag) 
        {
            Coordinate EndPoint = new Coordinate((int)sourceline.StartPoint.X, (int)sourceline.StartPoint.Y);
            Coordinate p2 = new Coordinate((int)sourceline.EndPoint.X, (int)sourceline.EndPoint.Y);
            
            int rd = resizelong;
            Vector2D start = new Vector2D(EndPoint.X, EndPoint.Y);
            Vector2D end = new Vector2D(p2.X, p2.Y);

            float angWithOx = (float)Math.Atan2(end.Y - start.Y, end.X - start.X);
            Vector2D onev = new Vector2D(Math.Cos(angWithOx), Math.Sin(angWithOx));

            // calculate start point of changed path 
            void ResizeStart()
            {
                EndPoint.X = EndPoint.X - (int)(onev.X * rd);
                EndPoint.Y = EndPoint.Y - (int)(onev.Y * rd);
            }
            // calculate final point of changed path 
            void ResizeEnd()
            {
                p2.X = p2.X + (int)(onev.X * rd);
                p2.Y = p2.Y + (int)(onev.Y * rd);
            }
            if (flag == 1)
            {
                ResizeStart();
                ResizeEnd();
            }
            if (flag == 2) ResizeStart();
            if (flag == 3) ResizeEnd();

            OsmNode[] nodes = new OsmNode[] { new OsmNode(EndPoint), new OsmNode(p2) };
            OsmWay OUT = new OsmWay(nodes);

            return OUT;
        }

    }
}

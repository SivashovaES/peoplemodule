﻿using System;
using NetTopologySuite.Geometries;
using OSMLSGlobalLibrary.Modules;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml.Serialization;
using System.Globalization;
using OSMLSGlobalLibrary;
using PathsFindingCoreModule;
using CityDataExpansion.OsmGeometris;
using CityDataExpansion;
using functoin;

namespace PeopleModule
{
    
    public class PeopleModule : OSMLSModule
    {
       // ========================================= Map objects =========================================
        /// <summary>
        /// All buildings
        /// </summary>
        public IReadOnlyCollection<OsmClosedWay> AllBuildings =>
            MapObjects.GetAll<OsmClosedWay>().Where(x => x.Tags.ContainsKey("building")).ToList().AsReadOnly();

        /// <summary>
        /// All roads
        /// </summary>
        public IReadOnlyCollection<OsmWay> AllHighways =>
            MapObjects.GetAll<OsmWay>().Where(x => x.Tags.ContainsKey("highway")).ToList().AsReadOnly();

        /// <summary>
        /// All railways
        /// </summary>
        public IReadOnlyCollection<OsmWay> AllRailways =>
            MapObjects.GetAll<OsmWay>().Where(x => x.Tags.ContainsKey("railway")).ToList().AsReadOnly();

        /// <summary>
        /// All cycleways
        /// </summary>
        public IReadOnlyCollection<OsmWay> AllCycleways =>
            MapObjects.GetAll<OsmWay>().Where(x => x.Tags.ContainsKey("cycleway")).ToList().AsReadOnly();

        /// <summary>
        /// Buildings
        /// </summary>
        private List<Building> Buildings { get; set; } = new List<Building>();

        /// <summary>
        /// Roads
        /// </summary>
        private List<OsmWay> Roads { get; set; } = new List<OsmWay>();


        // ============================== Params from param.ini ==============================
        /// <summary>
        /// Max count people on map
        /// </summary>
        private int MaxPeopleCount;

        /// <summary>
        /// Max count children on map
        /// </summary>
        private int MaxChildrenCount;

        /// <summary>
        /// Max count oldmen on map
        /// </summary>
        private int MaxOldmenCount;

        /// <summary>
        /// Max count grown-up men on map
        /// </summary>
        private int MaxGrownUpMenCount;

        /// <summary>
        /// Max count deviant behavior people on map
        /// </summary>
        private int MaxDeviantBehaviorPeopleCount;

        /// <summary>
        /// Max count poor health people on map
        /// </summary>
        private int MaxPoorHealthPeopleCount;

        /// <summary>
        /// Temperature of environment
        /// </summary>
        private int Temperature;

        /// <summary>
        /// Time of simulation
        /// </summary>
        private int TimeSimulation;

        // ============================= People on map =======================================

        /// <summary>
        /// List people on map
        /// </summary>
        private List<Character> Characters = new List<Character>();

        /// <summary>
        /// Count people on map
        /// </summary>
        private int ActorsCount;

        /// <summary>
        /// Count children on map
        /// </summary>
        private int ChildrenCount = 0;

        /// <summary>
        /// Count oldmen on map
        /// </summary>
        private int OldmenCount = 0;

        /// <summary>
        /// Count grown-up men on map
        /// </summary>
        private int GrownUpMenCount = 0;

        /// <summary>
        /// Count poor health people on map
        /// </summary>
        private int PoorHealthPeopleCount = 0;

        
        // ==========================  Other ================================

        /// <summary>
        /// Time since last character added
        /// </summary>
        private long LastCharacterAdded = 0;

        private Random Rnd;

        /// <summary>
        /// ID new actor on map
        /// </summary>
        private int ID;

        private List<List<int>> CollisionList = new List<List<int>>();

        private bool _isInitialized;

        private readonly object _initializationLock = new object();

        private OsmXml RawData { get; set; }

    
        // =============================== Methods ==========================================

        protected override void Initialize()
        {
            Rnd = new Random();
            lock (_initializationLock)
            {
                if (_isInitialized)
                {
                    throw new InvalidOperationException("Module already initialized.");
                }


                using (var streamReader = new StreamReader(OsmFilePath))
                {
                    RawData = (OsmXml)new XmlSerializer(typeof(OsmXml)).Deserialize(streamReader);
                }

                ParserObjectsOSM(RawData);


                // get buildings
                var tempBuildings = AllBuildings.ToArray<OsmClosedWay>();
                foreach(var build in tempBuildings)
                {
                    Buildings.Add(new Building(build.Nodes));
                }
                

                // get roads
                Roads = AllHighways.ToList<OsmWay>();

               _isInitialized = true;
               

                //create Entrances
                foreach (var building in Buildings)
                {
                    var index = Rnd.Next(building.Count - 2);
                    building.Entrance = ModuleMathExtensions.LineCenter(new LineSegment
                        (new Coordinate(building[index].X, building[index].Y), 
                        new Coordinate(building[index + 1].X, building[index + 1].Y)));
                }


                // read param.ini with params
                ReadSetting();

            }
        }

        /*protected override void Initialize()
        {
            #region создание базовых объектов
            // (Следует обратить внимание, что все координаты у всех геометрических объектов задаются в сферической проекции Меркатора (EPSG:3857).)
            // Создание координаты точки в начале координат.
            var pointCoordinate = new Coordinate(0, 0);
            // Создание стандартной точки в созданных ранее координатах.
            point = new Point(pointCoordinate);

            // Создание координат для линии.
            var lineCoordinates = new Coordinate[] {
                point.Coordinate,
                new Coordinate(0, -2000000),
                new Coordinate(-3000000, -1500000)
            };
            // Создание стандартной кривой линии по ранее созданным координатам.
            line = new LineString(lineCoordinates);

            // Создание координат полигона.
            var polygonCoordinates = new Coordinate[] {
                    new Coordinate(4000000, 5000000),
                    new Coordinate(6000000, 0),
                    new Coordinate(6000000, 6000000),
                    new Coordinate(4000000, 5000000)
            };
            // Создание стандартного полигона по ранее созданным координатам.
            polygon = new Polygon(new LinearRing(polygonCoordinates));

            #endregion

            #region создание базовых объектов

            // Добавление созданных объектов в общий список, доступный всем модулям. Объекты из данного списка отображаются на карте.
            MapObjects.Add(point);
            MapObjects.Add(line);
            MapObjects.Add(polygon);

            #endregion

            #region создание кастомного объекта и добавление на карту, модификация полигона заменой точки

            // Координаты самолёта, сконвертированные из Lat/Lon координат. Примерно в аэропорту "Internacional de Carrasco".
            var airplaneCoordinate = MathExtensions.LatLonToSpherMerc(-34.831747, -56.020034);
            // Скорость самолета, опять же, в сферической проекции Меркатора.
            var airplaneSpeed = 1000;
            // Создание объекта класса "самолет", реализованного в рамках данного модуля.
            airplane = new Airplane(airplaneCoordinate, airplaneSpeed);
            // Добавление самолета в список объектов.
            MapObjects.Add(airplane);

            // Заменим одну из точек ранее созданного полигона только что созданным самолетом.
            polygonCoordinates[2] = airplaneCoordinate;

            #endregion

            #region демонстрация получения данных из коллекции MapObjects
            // Коллекция MapObjects нужна не только для хранения и отображения объектов, но ещё и для удобного доступа к ним.

            // Попробуем получить все объекты, являющиеся строго точками.
            var onlyPoints = MapObjects.Get<Point>(); // Будет возвращена точка, созданная в самом начале.

            // А теперь получим все объекты, являющиеся точками и наследующиеся от точек (а также наследников наследников).
            var allPoints = MapObjects.GetAll<Point>(); // Будет возвращена точка, созданная в самом начале и самолет.

            // А теперь получим ВСЕ объекты на карте.
            var allMapObjects = MapObjects.GetAll<Geometry>(); // Будут возвращены все 4 созданных нами объекта.

            #endregion
        }*/

        /// <summary>
        /// Вызывается постоянно, здесь можно реализовывать логику перемещений и всего остального, требующего времени.
        /// </summary>
        /// <param name="elapsedMilliseconds">TimeNow.ElapsedMilliseconds</param>
        public override void Update(long elapsedMilliseconds)
        {
           
            bool collision;
            Coordinate CurrActPoint = new Coordinate(0, 0);
            List<LineString> paths = new List<LineString>();
            var delay = 1000; // generation frequency 

            // checking time of last actor generation
            if (elapsedMilliseconds - LastCharacterAdded > delay)
            {
                // comparing count actors on map with count from file
                if (Characters.Count < MaxPeopleCount)
                {
                    ChildrenCount = 0;
                    GrownUpMenCount = 0;
                    OldmenCount = 0;
                    PoorHealthPeopleCount = 0;
                    LastCharacterAdded = elapsedMilliseconds; // save time of generation
                    //count actors on map by age 
                    foreach (var ch in Characters.ToList())
                    {
                        if (ch.Age < 15) ChildrenCount += 1; 
                        if ((ch.Age > 16) & (ch.Age < 60)) GrownUpMenCount += 1; 
                        if (ch.Age > 60) OldmenCount += 1;
                    }
                    ActorsCount = Characters.Count;

                    // random selection of entrances buikdings
                    var sourceNode = Buildings[Rnd.Next(Buildings.Count() - 1)].Entrance; 
                    var targetNode = Buildings.Where(n => n.Entrance != sourceNode).ToList()[Rnd.Next(Buildings.Count() - 1)].Entrance;
                                                                                                                   
                    // get actor path from PathFinding module
                    paths.Add(PathsFinding.GetPath(sourceNode, targetNode, "Transport").Result);
                    MapObjects.Add(paths.Last<LineString>());
                    
                    // check if two people have same place of start point
                    if (!Character.CollisionCharaster(CurrActPoint, paths.Last<LineString>().Coordinate, 40))
                    {
                        int tempAge = GenActorAge();
                        bool health = GenActorHealth();
                        // create actor
                        Characters.Add(new Character(sourceNode.CoordinateValue, MapObjects.Get<LineString>().Last<LineString>(), 
                            CalcSpeed(tempAge, health), health, GenActorAge(), Roads.ToArray(), (ID += 1)));
                        MapObjects.Add(Characters.Last<Character>());
                    }
                }
            }


            try
            {
                foreach (var character in Characters.ToList())
                {
                    bool Contains = false;
                    bool remove;
                    CurrActPoint = character.Point;
                    foreach (var sourccharacter in Characters.ToList())
                    {
                        if (character.Deceleration < 10) character.Deceleration = character.Speed;
                        if (character != sourccharacter)
                        {
                            if (CollisionList.Count > 0)
                            {
                                // check if was collision with this actor
                                foreach (var c1 in CollisionList.ToList())
                                {
                                    if (c1 != null)
                                    {
                                        if ((c1.Contains(character.ID) & (c1.Contains(sourccharacter.ID))))
                                        {
                                            Contains = true;
                                            break;
                                        }
                                        else Contains = false;
                                    }
                                }
                            }
                            //check if two actors have collision at the moment
                            collision = Character.CollisionCharaster(character.Point, sourccharacter.Point, 10);
                            if (!Contains)
                            {
                                // actor is in collision at the moment and before wasn't in collision 
                                if (collision)
                                {   
                                    //save collision info
                                    List<int> Coll = new List<int>();
                                    Coll.Add(character.ID);
                                    Coll.Add(sourccharacter.ID);
                                    CollisionList.Add(Coll);
                                    int IndC, IndS;

                                    int X1 = (int)character.Point.X;
                                    int X2 = (int)character.NextPoint.X;
                                    int Y1 = (int)character.Point.Y;
                                    int Y2 = (int)character.NextPoint.Y;


                                    int X3 = (int)sourccharacter.Point.X;
                                    int X4 = (int)sourccharacter.NextPoint.X;
                                    int Y3 = (int)sourccharacter.Point.Y;
                                    int Y4 = (int)sourccharacter.NextPoint.Y;


                                    // actors on same longitude
                                    if ((Y1 == Y2) == (Y2 == Y3))
                                    {
                                        // both actors go left or right (have same direction) 
                                        if ((((X1 < X2) & (X1 < X3)) & (X3 < X4))
                                            || (((X1 > X2) & (X1 > X3)) & (X3 > X4)))
                                        {
                                            character.Deceleration = sourccharacter.Deceleration;
                                        }
                                       
                                        int deltaX = 0, deltaY = 0;

                                        // actors go towards each other
                                        // one actor goes right
                                        if (((X1 > X2) & (X1 > X3)) & (X3 < X4))
                                        {
                                            deltaX = -5;
                                            deltaY = 5;   
                                        }
                                        // one actor goes left
                                        if ((((X1 < X2) & (X1 < X3)) & (X3 > X4)))
                                        {
                                            deltaX = 5;
                                            deltaY = -5;
                                        }
                                        // moving people so that they do not collide
                                        if (deltaX != 0 || deltaY != 0)
                                        {
                                            List<Coordinate> buf = character.Path.Coordinates.ToList<Coordinate>();
                                            IndC = buf.IndexOf(character.NextPoint);
                                            character.NextPoint = new Coordinate(X1 + deltaX, Y1 + deltaY);
                                            buf.Insert(IndC, new Coordinate(character.NextPoint));
                                            character.Path = new LineString(buf.ToArray());
                                            buf.Clear();
                                        }

                                    }
                                    // actors on same latitude
                                    if (((X1 == X2) == (X3 == X4)))
                                    {
                                        // both actors go up or down (have same direction)
                                        if ((((Y1 < Y2) & (Y1 < Y3)) & (Y3 < Y4)) &&
                                            (((Y1 > Y2) & (Y1 > Y3)) & (Y3 > Y4)))
                                        {
                                            character.Deceleration = sourccharacter.Deceleration;

                                        }

                                        int deltaX = 0, deltaY = 0;

                                        // actors go towards to each other
                                        // one actor goes down
                                        if (((Y1 > Y2) & (Y1 > Y3)) & (Y3 < Y4))
                                        {
                                            deltaX = -5;
                                            deltaY = -5;
                                        }
                                        // one actor goes up
                                        if ((((Y1 < Y2) & (Y1 < Y3)) & (Y3 > Y4)))
                                        {
                                            deltaX = 5;
                                            deltaY = 5;
                                        }
                                        // moving people so that they do not collide
                                        if (deltaX != 0 || deltaY != 0)
                                        {
                                            List<Coordinate> buf = character.Path.Coordinates.ToList<Coordinate>();
                                            IndC = buf.IndexOf(character.NextPoint);
                                            character.NextPoint = new Coordinate(X1 + deltaX, Y1 + deltaY);
                                            buf.Insert(IndC, new Coordinate(character.NextPoint));
                                            character.Path = new LineString(buf.ToArray());
                                            buf.Clear();
                                        }

                                    }
                                    if ((X1 != X2) & (Y1 != Y2))
                                    {
                                        List<Coordinate> buf = character.Path.Coordinates.ToList<Coordinate>();
                                        IndC = buf.IndexOf(character.NextPoint);
                                        character.NextPoint = new Coordinate(X1 - 5, Y1 + 5);
                                        buf.Insert(IndC, new Coordinate(character.NextPoint));
                                        character.Path = new LineString(buf.ToArray());
                                        buf.Clear();
                                        buf = sourccharacter.Path.Coordinates.ToList<Coordinate>();   
                                        IndS = buf.IndexOf(sourccharacter.NextPoint);
                                        sourccharacter.NextPoint = new Coordinate(X1 + 5, Y1 - 5);
                                        buf.Insert(IndC, new Coordinate(sourccharacter.NextPoint));
                                        sourccharacter.Path = new LineString(buf.ToArray());
                                    }


                                }


                            }
                            else
                            {
                                collision = Character.CollisionCharaster(character.Point, sourccharacter.Point, 15);
                                // if there isn't collision at the moment - delete it from list
                                if (!collision) 
                                {
                                    character.Deceleration = character.Speed;
                                    sourccharacter.Deceleration = sourccharacter.Speed;
                                    foreach (var c1 in CollisionList.ToList())
                                    {
                                        if (c1 != null)
                                        {
                                            int Index = CollisionList.IndexOf(c1);
                                            if (c1.Contains(character.ID) & (c1.Contains(sourccharacter.ID)))
                                            {
                                                CollisionList.RemoveAt(Index);
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }

                    character.Update(out remove);
                    if (remove)
                    {
                        MapObjects.Remove(character.Path);
                        MapObjects.Remove(character);
                        Characters.Remove(character);
                    }

                }
            }
            catch (Exception)
            {

            }
        }

        public void ParserObjectsOSM(OsmXml OSMData)
        {
            var coordinateboundsх = MathExtensions.LatLonToSpherMerc(
                    double.Parse(OSMData.Bounds.Minlat, NumberStyles.Any, CultureInfo.InvariantCulture),
                    double.Parse(OSMData.Bounds.Minlon, NumberStyles.Any, CultureInfo.InvariantCulture)
            );
            var coordinateboundsy = MathExtensions.LatLonToSpherMerc(
                   double.Parse(OSMData.Bounds.Maxlat, NumberStyles.Any, CultureInfo.InvariantCulture),
                   double.Parse(OSMData.Bounds.Maxlon, NumberStyles.Any, CultureInfo.InvariantCulture)
           );

            Coordinate[] arrbycoordinate = { coordinateboundsх,
                new Coordinate(coordinateboundsх.X, coordinateboundsy.Y),
                coordinateboundsy,
                new Coordinate(coordinateboundsy.X, coordinateboundsх.Y),
                coordinateboundsх };

            LinearRing allcoordinatesbounds = new LinearRing(arrbycoordinate);

            var mapNodes = OSMData.Node.ToDictionary(
                node => node.Id,
                node =>
                {
                    var coordinate = MathExtensions.LatLonToSpherMerc(
                        double.Parse(node.Lat, NumberStyles.Any, CultureInfo.InvariantCulture),
                        double.Parse(node.Lon, NumberStyles.Any, CultureInfo.InvariantCulture)
                        );

                    return SetTags(new OsmNode(coordinate), node.Tag);
                }
            );

            var mapWays = OSMData.Way.Where(way => way.Nd.First().Ref != way.Nd.Last().Ref).ToDictionary(
                way => way.Id,
                way =>
                {
                    List<OsmNode> wayNodes = new List<OsmNode>();
                    foreach (var nodeId in way.Nd.Select(x => x.Ref))
                    {
                        if (mapNodes.ContainsKey(nodeId))
                        {
                            wayNodes.Add(mapNodes[nodeId]);
                        }
                    }

                    return SetTags(new OsmWay(wayNodes.ToArray()), way.Tag);
                });

            var mapAreas = OSMData.Way.Where(way => way.Nd.First().Ref == way.Nd.Last().Ref).ToDictionary(
                way => way.Id,
                way =>
                {
                    List<OsmNode> wayNodes = new List<OsmNode>();
                    foreach (var nodeId in way.Nd.Select(x => x.Ref))
                    {
                        if (mapNodes.ContainsKey(nodeId))
                        {
                            wayNodes.Add(mapNodes[nodeId]);
                        }
                    }

                    return
                         SetTags(new OsmClosedWay(wayNodes.ToArray()),
                        way.Tag);
                });

            var mapRelations = new Dictionary<string, OsmRelation>();
            foreach (var relation in OSMData.Relation)
            {
                mapRelations[relation.Id] = SetTags(new OsmRelation(
                    relation.Member.Select(
                        member =>
                        {
                            Geometry geometry = null;

                            if (member.Type == "node")
                            {
                                if (mapNodes.ContainsKey(member.Ref))
                                    geometry = mapNodes[member.Ref];
                            }
                            else if (member.Type == "way")
                            {
                                if (mapWays.ContainsKey(member.Ref))
                                    geometry = mapWays[member.Ref];
                                else if (mapAreas.ContainsKey(member.Ref))
                                    geometry = mapAreas[member.Ref];
                            }
                            else if (member.Type == "relation")
                            {
                                if (mapRelations.ContainsKey(member.Ref))
                                    geometry = mapRelations[member.Ref];
                            }

                            return (geometry, member.Role);
                        }).Where(x => x.geometry != null).ToList()
                    ), relation.Tag);
            }
        }

        T SetTags<T>(T geometry, List<TagXml> rawTags) where T : Geometry, IOsmObject
        {
            foreach (var tag in rawTags)
            {
                geometry.Tags[tag.K] = tag.V;
            }

            if (geometry.Tags.Count != 0)
            {
                MapObjects.Add(geometry);
            }

            return geometry;
        }


        public void ReadSetting()
        {
            INIManager ini;
            try
            {
                string fileIni = (@".\\modules\\param.ini");
                ini = new INIManager(fileIni);
                MaxPeopleCount = Convert.ToInt32(ini.GetPrivateString("PARAMACTOR", "actors"));
                MaxChildrenCount = Convert.ToInt32(ini.GetPrivateString("PARAMACTOR", "children"));
                MaxGrownUpMenCount = Convert.ToInt32(ini.GetPrivateString("PARAMACTOR", "growupmen"));
                MaxOldmenCount = Convert.ToInt32(ini.GetPrivateString("PARAMACTOR", "oldmen"));
                MaxDeviantBehaviorPeopleCount = Convert.ToInt32(ini.GetPrivateString("PARAMACTOR", "deviantbehaivor"));
                MaxPoorHealthPeopleCount = Convert.ToInt32(ini.GetPrivateString("PARAMACTOR", "poorhealth"));
                Temperature = Convert.ToInt32(ini.GetPrivateString("PARAMACTOR", "temperature"));
                TimeSimulation = Convert.ToInt32(ini.GetPrivateString("PARAMACTOR", "time"));
                ini = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка чтения файла настроек, проверьте путь до файла и его структуру {0}", e);
                ini = null;
            }

        }

        public int GenActorAge()
        {
            Random rn = new Random();

            if (Characters.Count != 0)
            {
                int i = rn.Next(0, 3);
                switch (i)
                {
                    case 1:
                        {
                            if (ChildrenCount < MaxChildrenCount)
                                return rn.Next(5, 14);
                            break;
                        }
                    case 2:
                        {
                            if (GrownUpMenCount < MaxGrownUpMenCount) 
                                return rn.Next(15, 59);
                            break;
                        }
                    case 3:
                        {
                            if (OldmenCount < MaxOldmenCount) 
                                return rn.Next(60, 100);
                            break;
                        }
                    default:
                        {
                            return rn.Next(5, 100);
                            
                        }
                }
            }
            return rn.Next(5, 100);
        }

        public static int CalcSpeed(int age, bool health)
        {
            int speed = 1;

            Random rndSpeed = new Random();
            if ((0 < age) & (age <= 100))
            {
                if (((age < 12)) || ((age > 59)) || health) speed = rndSpeed.Next(1, 10);
                if ((((age < 12)) || ((age > 59))) && health) speed = rndSpeed.Next(1, 5);
                if ((16 < age) && (60 > age)) speed = rndSpeed.Next(5, 30);
            }
            else speed = rndSpeed.Next(1, 30);
            return speed;
        }

        public bool GenActorHealth()
         {
            // 1 - good, 0 - bad

            int health = 0;

            if (PoorHealthPeopleCount < MaxPoorHealthPeopleCount)
                health = Rnd.Next(0, 2);

            return (health == 0);
         }
    }
}